package lv.odo.bootcamp.yara;

import static org.junit.Assert.assertEquals;

import java.util.Date;
import org.apache.log4j.Logger;

import org.junit.Test;

import junit.framework.AssertionFailedError;
import lv.odo.bootcamp.yara.entities.Feed;

public class FeedTest {
	private static Logger logger = Logger.getLogger(FeedTest.class.getName());

	@Test
	public void test() throws Exception {
		String url = "http://google.com";
		String urlicon = "http://google.com/icon.ico";
		String title = "google";
		int id = 5;
		Date date = new Date();
		try {
			Feed feed = new Feed();
			logger.info("Feed object created");

			feed.setFeedUrl(url);
			feed.setIconUrl(urlicon);
			feed.setTitle(title);
			feed.setId(id);
			feed.setLastUpdated(date);
			logger.info("Feed object initiated");

			assertEquals(url, feed.getFeedUrl());
			assertEquals(urlicon, feed.getIconUrl());
			assertEquals(title, feed.getTitle());
			assertEquals(id, feed.getId());
			assertEquals(date, feed.getLastUpdated());
			logger.info("Feed object Get methods passed!");

			feed.setFeedUrl("testurl");
			assertEquals("testurl", feed.getFeedUrl());
			feed.setIconUrl("testico");
			assertEquals("testico", feed.getIconUrl());
			feed.setTitle("testtitle");
			assertEquals("testtitle", feed.getTitle());
			feed.setId(9001);
			assertEquals(9001, feed.getId());
			Date testDate = new Date();
			feed.setLastUpdated(testDate);
			assertEquals(testDate, feed.getLastUpdated());
			logger.info("Feed object Set methods passed!");

		} catch (AssertionFailedError e) {
			logger.error("Failed" + e);
		}
	}

}

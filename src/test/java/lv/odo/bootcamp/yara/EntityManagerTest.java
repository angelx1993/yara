package lv.odo.bootcamp.yara;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import lv.odo.bootcamp.yara.entities.Feed;
import lv.odo.bootcamp.yara.entities.FeedEntry;

public class EntityManagerTest {

	private static Logger logger = Logger.getLogger(EntityManagerTest.class.getName());
	private static final Random rand = new Random();
	private static LinkedList<Feed> feeds = new LinkedList<>();
	private static LinkedList<FeedEntry> feedEntries = new LinkedList<>();
	private Session session;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		@SuppressWarnings("unused")
		EntityManager entityManager = new EntityManager();
		// Session session = HibernateUtil.getSession();
		// session.createQuery("truncate yara_feed_entries").executeUpdate();
		// session.createQuery("truncate yara_feeds").executeUpdate();
		// session.close();
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		Session session = HibernateUtil.getSession();
		for(Feed feed : feeds) {
			session.createQuery("delete yara_feed_entries where feed_id=" + feed.getId()).executeUpdate();
			session.createQuery("delete yara_feeds where id=" + feed.getId()).executeUpdate();
		}
		for(FeedEntry entry : feedEntries) {
			session.createQuery("delete yara_feed_entries where feed_id=" + entry.getId()).executeUpdate();
		}
	}

	@Before
	public void setUp() throws Exception {
		session = HibernateUtil.getSession();
	}

	@After
	public void tearDown() throws Exception {
		if (session.isConnected())
			session.close();
	}

	@Test
	public void testSaveFeed() {
		String feedTitle = "odo.lv | Global RSS";
		String feedUrl = "http://odo.lv/WebRss?xpage=plain";
		String iconUrl = "http://odo.lv/favicon.ico";
		Date lastUpdated = new Date(0);

		Feed feed = new Feed();
		feed.setFeedUrl(feedUrl);
		feed.setIconUrl(iconUrl);
		feed.setLastUpdated(lastUpdated);
		feed.setTitle(feedTitle);

		try {
			EntityManager.saveFeed(feed);
		} catch (Exception e) {
			logger.error("FAILED " + e);
			fail();
		}

		Query Query = session.createQuery("from yara_feeds");
		int id = -1;
		for (Object f : Query.list()) {
			feed = (Feed) f;
			if (feed.getTitle().equals(feedTitle)) {
				id = feed.getId();
				break;
			}
		}
		assertNotEquals(-1, id);

		logger.info("OK");
	}

	@Test
	public void testGetFeed() {
		String feedTitle = new BigInteger(130, rand).toString(32);
		String feedUrl = "http://habrahabr.ru/rss";
		String iconUrl = "http://habrahabr.ru/favicon.ico";
		Date lastUpdated = new Date(rand.nextInt());

		Feed feed = new Feed();
		feed.setFeedUrl(feedUrl);
		feed.setIconUrl(iconUrl);
		feed.setLastUpdated(lastUpdated);
		feed.setTitle(feedTitle);

		session.save(feed);
		feeds.add(feed);

		Query Query = session.createQuery("from yara_feeds");
		int id = -1;
		for (Object f : Query.list()) {
			feed = (Feed) f;
			if (feed.getTitle().equals(feedTitle)) {
				id = feed.getId();
				break;
			}
		}
		assertNotEquals(-1, id);

		session.close();
		Feed result = null;
		try {
			result = EntityManager.getFeed(id);
		} catch (Exception e) {
			logger.error("FAILED " + e);
			fail();
		}
		assertNotNull(result);
		assertEquals(feedTitle, result.getTitle());
		assertEquals(feedUrl, result.getFeedUrl());
		assertEquals(iconUrl, result.getIconUrl());
		assertEquals(lastUpdated, result.getLastUpdated());
		logger.info("OK");
	}

	@Test
	public void testDeleteFeed() {
		String feedTitle = new BigInteger(130, rand).toString(32);
		String feedUrl = "http://habrahabr.ru/rss";
		String iconUrl = "http://habrahabr.ru/favicon.ico";
		Date lastUpdated = new Date(rand.nextInt());

		Feed feed = new Feed();
		feed.setFeedUrl(feedUrl);
		feed.setIconUrl(iconUrl);
		feed.setLastUpdated(lastUpdated);
		feed.setTitle(feedTitle);

		session.save(feed);
		feeds.add(feed);

		Query Query = session.createQuery("from yara_feeds");
		int id = -1;
		for (Object f : Query.list()) {
			feed = (Feed) f;
			if (feed.getTitle().equals(feedTitle)) {
				id = feed.getId();
				break;
			}
		}
		assertNotEquals(-1, id);

		try {
			EntityManager.deleteFeed(id);
		} catch (Exception e) {
			logger.error("FAILED " + e);
			fail();
		}
		logger.info("OK");
	}

	@Test
	public void testGetFeedList() {
		ArrayList<String> titles = new ArrayList<>();

		String feedUrl = "http://habrahabr.ru/rss";
		String iconUrl = "http://habrahabr.ru/favicon.ico";
		Date lastUpdated = new Date(rand.nextInt());

		Feed feed = null;
		String feedTitle = null;
		int feedAmount = rand.nextInt(20);
		for (int i = 0; i < feedAmount; i++) {
			feedTitle = new BigInteger(130, rand).toString(32);
			feed = new Feed();
			feed.setFeedUrl(feedUrl);
			feed.setIconUrl(iconUrl);
			feed.setLastUpdated(lastUpdated);
			feed.setTitle(feedTitle);
			titles.add(feedTitle);
			session.save(feed);
			feeds.add(feed);
		}

		List<Feed> result = null;
		try {
			result = EntityManager.getFeedList();
		} catch (Exception e) {
			logger.error("FAILED " + e);
			fail();
		}
		assertNotNull(result);
		assertNotEquals(0, result.size());
		// assertFalse(result.size() < titles.size());
		logger.info("OK");
	}

	@Test
	public void testSaveFeedEntry() {
		String title = new BigInteger(130, rand).toString(32);
		String content = new BigInteger(1024, rand).toString(32);
		String link = new BigInteger(130, rand).toString(32);
		Date pubDate = new Date(rand.nextInt());

		FeedEntry feedEntry = new FeedEntry();
		feedEntry.setTitle(title);
		feedEntry.setContent(content);
		feedEntry.setLink(link);
		feedEntry.setPublicationDate(pubDate);

		try {
			EntityManager.saveFeedEntry(feedEntry);
			feedEntries.add(feedEntry);
		} catch (Exception e) {
			logger.error("FAILED " + e);
			fail();
		}

		Query Query = session.createQuery("from yara_feed_entries");
		int id = -1;
		for (Object entry : Query.list()) {
			feedEntry = (FeedEntry) entry;
			if (feedEntry.getTitle().equals(title)) {
				id = feedEntry.getId();
				break;
			}
		}
		assertNotEquals(-1, id);
		logger.info("OK");
	}

	@Test
	public void testGetFeedEntry() {
		String title = new BigInteger(130, rand).toString(32);
		String content = new BigInteger(1024, rand).toString(32);
		String link = new BigInteger(130, rand).toString(32);
		Date pubDate = new Date(rand.nextInt());

		String feedTitle = new BigInteger(130, rand).toString(32);
		String feedUrl = "http://habrahabr.ru/rss";
		String iconUrl = "http://habrahabr.ru/favicon.ico";
		Date lastUpdated = new Date(rand.nextInt());

		Feed feed = new Feed();
		feed.setTitle(feedTitle);
		feed.setFeedUrl(feedUrl);
		feed.setIconUrl(iconUrl);
		feed.setLastUpdated(lastUpdated);

		FeedEntry feedEntry = new FeedEntry();
		feedEntry.setTitle(title);
		feedEntry.setContent(content);
		feedEntry.setLink(link);
		feedEntry.setPublicationDate(pubDate);
		feedEntry.setFeed(feed);

		session.save(feed);
		feeds.add(feed);
		session.save(feedEntry);
		feedEntries.add(feedEntry);

		Query Query = session.createQuery("from yara_feed_entries");
		int id = -1;
		for (Object entry : Query.list()) {
			feedEntry = (FeedEntry) entry;
			if (feedEntry.getTitle().equals(title)) {
				id = feedEntry.getId();
				break;
			}
		}
		assertNotEquals(-1, id);

		session.close();

		FeedEntry result = null;
		try {
			result = EntityManager.getFeedEntry(id);
		} catch (Exception e) {
			logger.error("FAILED " + e);
			fail();
		}
		assertNotNull(result);
		assertEquals(content, result.getContent());
		assertNotNull(result.getFeed());
		assertEquals(feedTitle, result.getFeed().getTitle());
		assertEquals(link, result.getLink());
		assertEquals(pubDate, result.getPublicationDate());
		assertEquals(title, result.getTitle());
		logger.info("OK");
	}

	@Test
	public void testGetFeedUnreadCount() {
		String feedTitle = new BigInteger(130, rand).toString(32);
		String feedUrl = "http://habrahabr.ru/rss";
		String iconUrl = "http://habrahabr.ru/favicon.ico";
		Date lastUpdated = new Date(rand.nextInt());

		Feed feed = new Feed();
		feed.setTitle(feedTitle);
		feed.setFeedUrl(feedUrl);
		feed.setIconUrl(iconUrl);
		feed.setLastUpdated(lastUpdated);
		session.save(feed);
		feeds.add(feed);

		int amount = rand.nextInt(20);
		long unreadCount = 0;
		for (int i = 0; i < amount; i++) {
			String title = new BigInteger(130, rand).toString(32);
			String content = new BigInteger(1024, rand).toString(32);
			String link = new BigInteger(130, rand).toString(32);
			Date pubDate = new Date(rand.nextInt());
			boolean isRead = (rand.nextInt(2) == 1);

			FeedEntry feedEntry = new FeedEntry();
			feedEntry.setTitle(title);
			feedEntry.setContent(content);
			feedEntry.setLink(link);
			feedEntry.setPublicationDate(pubDate);
			feedEntry.setFeed(feed);
			feedEntry.setRead(isRead);
			if (!isRead)
				unreadCount++;
			session.save(feedEntry);
			feedEntries.add(feedEntry);
		}
		session.close();
		long result = -1;
		try {
			result = EntityManager.getFeedUnreadCount(feed);
		} catch (Exception e) {
			logger.error("FAILED " + e);
			fail();
		}
		assertNotEquals(-1, result);
		assertEquals(unreadCount, result);
		logger.info("OK");
	}

	@Test
	public void testGetFeedEntryListFeed() {
		String feedTitle = new BigInteger(130, rand).toString(32);
		String feedUrl = "http://habrahabr.ru/rss";
		String iconUrl = "http://habrahabr.ru/favicon.ico";
		Date lastUpdated = new Date(rand.nextInt());

		Feed feed = new Feed();
		feed.setTitle(feedTitle);
		feed.setFeedUrl(feedUrl);
		feed.setIconUrl(iconUrl);
		feed.setLastUpdated(lastUpdated);
		session.save(feed);
		feeds.add(feed);

		ArrayList<FeedEntry> entries = new ArrayList<>();
		int amount = rand.nextInt(20);
		for (int i = 0; i < amount; i++) {
			String title = new BigInteger(130, rand).toString(32);
			String content = new BigInteger(1024, rand).toString(32);
			String link = new BigInteger(130, rand).toString(32);
			Date pubDate = new Date(rand.nextInt());
			boolean isRead = (rand.nextInt(2) == 1);

			FeedEntry feedEntry = new FeedEntry();
			feedEntry.setTitle(title);
			feedEntry.setContent(content);
			feedEntry.setLink(link);
			feedEntry.setPublicationDate(pubDate);
			feedEntry.setFeed(feed);
			feedEntry.setRead(isRead);
			entries.add(feedEntry);
			session.save(feedEntry);
			feedEntries.add(feedEntry);
		}
		session.close();
		List<FeedEntry> result = EntityManager.getFeedEntryList(feed);
		assertEquals(entries.size(), result.size());
		logger.info("OK");
	}

	@Test
	public void testExceptionSaveFeed() {
		Feed feed = new Feed();
		boolean result = EntityManager.saveFeed(feed);
		assertFalse(result);
	}

	@Test
	public void testExceptionSaveFeedEntry() {
		FeedEntry feedEntry = new FeedEntry();
		boolean result = EntityManager.saveFeedEntry(feedEntry);
		assertFalse(result);
	}

}

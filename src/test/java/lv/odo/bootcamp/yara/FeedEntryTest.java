package lv.odo.bootcamp.yara;

import static org.junit.Assert.*;

import java.util.Date;
import org.apache.log4j.Logger;

import org.junit.Assert;
import org.junit.Test;

import junit.framework.AssertionFailedError;
import lv.odo.bootcamp.yara.entities.Feed;
import lv.odo.bootcamp.yara.entities.FeedEntry;

public class FeedEntryTest {
	private static Logger logger = Logger.getLogger(FeedEntryTest.class.getName());

	@Test
	public void test() {

		String title = "google";
		String content = "wadawdawodpoa poadkpoa poawkdpaw dl;aw,l ,dl;w,dlwa d,wakd[ dwad[,[aw,daw[,d[wa,d[p[aw";
		String url = "http://google.com";
		int id = 5;
		Date date = new Date();
		Feed feed = new Feed();
		try {
			FeedEntry feedEntry = new FeedEntry();
			logger.info("FeedEntry object created!");
			feedEntry.setContent(content);
			feedEntry.setFeed(feed);
			feedEntry.setId(id);
			feedEntry.setLink(url);
			feedEntry.setTitle(title);
			feedEntry.setPublicationDate(date);
			feedEntry.setRead(false);
			logger.info("FeedEntry object initiated!");

			assertEquals(url, feedEntry.getLink());
			assertEquals(content, feedEntry.getContent());
			assertEquals(title, feedEntry.getTitle());
			assertEquals(id, feedEntry.getId());
			assertEquals(date, feedEntry.getPublicationDate());
			assertEquals(feed, feedEntry.getFeed());
			assertEquals(false, feedEntry.isRead());
			logger.info("FeedEntry object Get methods passed!");

			feedEntry.setLink("testlink");
			assertEquals("testlink", feedEntry.getLink());
			feedEntry.setRead(true);
			assertEquals(true, feedEntry.isRead());
			assertEquals("testlink", feedEntry.getLink());
			feedEntry.setContent("CCCCCCCCCCCCCCCCCCCCCCCSSSSSSSSSSSSSSSSSSSSSSSSSSDDDDDDD");
			assertEquals("CCCCCCCCCCCCCCCCCCCCCCCSSSSSSSSSSSSSSSSSSSSSSSSSSDDDDDDD", feedEntry.getContent());
			feedEntry.setTitle("testTitle");
			assertEquals("testTitle", feedEntry.getTitle());
			feedEntry.setId(1337);
			assertEquals(1337, feedEntry.getId());
			Date testDate = new Date();
			feedEntry.setPublicationDate(testDate);
			assertEquals(testDate, feedEntry.getPublicationDate());
			Feed feed2 = new Feed();
			feed2.setId(1);
			feedEntry.setFeed(feed2);
			assertEquals(feed2, feedEntry.getFeed());
			logger.info("FeedEntry object Set methods passed!");
		} catch (AssertionFailedError e) {
			logger.error("FAILED " + e);
			Assert.fail();
		}
	}

}

package lv.odo.bootcamp.yara;

import java.util.Date;

import org.junit.BeforeClass;
import org.junit.Test;

import lv.odo.bootcamp.yara.entities.Feed;

public class RefreshFeedThreadTest {
	public static Feed feed;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		feed = new Feed();
		feed.setFeedUrl("http://www.delfi.lv/rss.php");
		feed.setLastUpdated(new Date(0));
	}

	@Test
	public void test() {
		RefreshFeedThread refreshFeedThread = new RefreshFeedThread(feed);
		refreshFeedThread.run();
	}


}

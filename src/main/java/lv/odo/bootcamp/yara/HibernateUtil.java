package lv.odo.bootcamp.yara;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;

/**
 * Manages Hibernate session factory
 *
 */
public class HibernateUtil {
	private static final SessionFactory sessionFactory;

	static {
		Configuration configuration = new Configuration().configure();
		ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder()
				.applySettings(configuration.getProperties()).build();
		sessionFactory = configuration.buildSessionFactory(serviceRegistry);
	}

	/**
	 * Returns a new session
	 * 
	 * @return A new session
	 */
	public static Session getSession() {
		return sessionFactory.openSession();
	}
}

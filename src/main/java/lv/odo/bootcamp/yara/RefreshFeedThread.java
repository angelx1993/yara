package lv.odo.bootcamp.yara;

import lv.odo.bootcamp.yara.entities.Feed;

/**
 * class to be excecuted by a thread and refresh the feeds.
 * 
 * @author student
 *
 */
public class RefreshFeedThread implements Runnable {
	Feed feed;

	/**
	 * defines local variables.
	 * 
	 * @param feed
	 */
	public RefreshFeedThread(Feed feed) {
		this.feed = feed;
	}

	/**
	 * function to be excecuted by a thread.
	 */
	@Override
	public void run() {
		RssFetcher.fetchEntries(feed);
	}

}

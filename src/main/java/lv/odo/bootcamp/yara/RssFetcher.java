package lv.odo.bootcamp.yara;

import java.io.FileNotFoundException;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import lv.odo.bootcamp.yara.entities.Feed;
import lv.odo.bootcamp.yara.entities.FeedEntry;
import lv.odo.bootcamp.yara.gui.FeedTreeRenderer;

public class RssFetcher {
	private static Logger logger = Logger.getLogger(EntityManager.class.getName());

	public RssFetcher() {
	}

	/**
	 * fetches feed data, defines a feed.
	 * 
	 * @param feed
	 *            feed to be defined.
	 */
	public static void fetchEntries(Feed feed) {
		Date dateLastUpdated = feed.getLastUpdated();
		Date dateLastEntry = null;
		URL rssFeedUrl = null;
		String iconUrl = null;
		try {
			rssFeedUrl = new URL(feed.getFeedUrl());
		} catch (MalformedURLException e1) {
			logger.error("fetchEntries URL ERROR: " + e1);
			e1.printStackTrace();
		}
		try {
			DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
			Document doc = builder.parse(rssFeedUrl.openStream());

			if (feed.getLastUpdated().equals(new Date(0))) {
				feed.setTitle(doc.getElementsByTagName("title").item(0).getFirstChild().getNodeValue());

				// if rssFeedUrl starts with "feeds."
				if (rssFeedUrl.getHost().substring(0, 6).matches("^feeds.")) {
					// cuts String "feeds." off of URl
					String website = rssFeedUrl.getHost().substring(6);
					URL url = new URL("http://" + website);
					iconUrl = "http://www.google.com/s2/favicons?domain=" + url;

					feed.setIconUrl(iconUrl);
					// else checks if the url contains a favicon.ico
				} else {
					iconUrl = rssFeedUrl.getProtocol() + "://" + rssFeedUrl.getHost() + "/favicon.ico";
					// checks if iconUrl contains favicon
					try {
						FeedTreeRenderer.readIcon(new URL(iconUrl));
						feed.setIconUrl(iconUrl);
					} catch (FileNotFoundException e) {
						logger.error("ERROR: " + e);
						feed.setIconUrl(null);
					}
				}
			}
			EntityManager.saveFeed(feed);

			NodeList items = doc.getElementsByTagName("item");
			for (int i = 0; i < items.getLength(); i++) {
				Element item = (Element) items.item(i);
				FeedEntry feedEntry = new FeedEntry();
				feedEntry.setTitle(getValue(item, "title"));
				feedEntry.setLink(getValue(item, "link"));
				feedEntry.setContent(getValue(item, "description"));

				String strDate = getValue(item, "pubDate");
				Date date = new Date();
				SimpleDateFormat dateformat = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss Z");
				date = dateformat.parse(strDate);
				if (i == 0)
					dateLastEntry = date;

				feedEntry.setPublicationDate(date);
				feedEntry.setFeed(feed);
				if (dateLastUpdated.before(feedEntry.getPublicationDate())) {
					EntityManager.saveFeedEntry(feedEntry);
				} else
					break;
			}

			feed.setLastUpdated(dateLastEntry);
			EntityManager.saveFeed(feed);
		} catch (Exception e) {
			logger.error("fetchEntries ERROR: " + e);
		}

	}

	/**
	 * get values of a node.
	 * 
	 * @param parent
	 *            parent node.
	 * @param nodeName
	 *            node.
	 * @return returns the value of a node.
	 */
	public static String getValue(Element parent, String nodeName) {
		return parent.getElementsByTagName(nodeName).item(0).getFirstChild().getNodeValue();
	}

}

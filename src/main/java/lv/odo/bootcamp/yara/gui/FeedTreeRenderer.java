package lv.odo.bootcamp.yara.gui;

import java.awt.Component;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JTree;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeCellRenderer;

import org.apache.log4j.Logger;

import lv.odo.bootcamp.yara.EntityManager;
import lv.odo.bootcamp.yara.entities.Feed;
import net.sf.image4j.codec.ico.ICODecoder;

/**
 * adds icons to the feed list
 * 
 * @author student
 *
 */
public class FeedTreeRenderer extends DefaultTreeCellRenderer {
	private static Logger logger = Logger.getLogger(FeedTreeRenderer.class.getName());
	private static final long serialVersionUID = 1L;
	private JLabel label;

	/**
	 * Constructor defines label
	 */
	public FeedTreeRenderer() {
		label = new JLabel();
	}

	/**
	 * sets icon into the list.
	 */
	public Component getTreeCellRendererComponent(JTree tree, Object value, boolean selected, boolean expanded,
			boolean leaf, int row, boolean hasFocus) {
		Object o = ((DefaultMutableTreeNode) value).getUserObject();
		BufferedImage img = null;
		Image image = null;
		if (o instanceof Feed) {
			Feed feed = (Feed) o;
			// if the url contains a string that is not null
			if (feed.getIconUrl() != null) {
				// if url starts with "http://www.google", then its a .png
				if (feed.getIconUrl().substring(0, 17).matches("^http://www.google")) {
					try {
						img = readPngIcon(new URL(feed.getIconUrl()));
						image = img.getScaledInstance(20, 20, Image.SCALE_SMOOTH);
					} catch (MalformedURLException e) {
						logger.error("ERROR: " + e);
						e.printStackTrace();
					}
					// else the url contains a .ico
				} else {
					try {
						img = readIcon(new URL(feed.getIconUrl()));
						image = img.getScaledInstance(20, 20, Image.SCALE_SMOOTH);
					} catch (MalformedURLException e) {
						logger.error("ERROR: " + e);
						e.printStackTrace();
					} catch (IOException e) {
						logger.error("ERROR: " + e);
						e.printStackTrace();
					}
				}
				// else set default icon
			} else {
				try {
					feed.setIconUrl("http://www.feedicons.com/favicon.ico");
					img = readIcon(new URL(feed.getIconUrl()));
					image = img.getScaledInstance(24, 24, Image.SCALE_SMOOTH);
				} catch (MalformedURLException e) {
					logger.error("ERROR: " + e);
					e.printStackTrace();
				} catch (IOException e) {
					logger.error("ERROR: " + e);
					e.printStackTrace();
				}
			}

			if (image != null) {
				label.setIcon(new ImageIcon(image));
			}
			label.setText(feed.getTitle() + " (" + EntityManager.getFeedUnreadCount(feed) + ")");
		} else {
			label.setIcon(null);
			label.setText("" + value);
		}
		return label;
	}

	/**
	 * gets imagfrom the web.
	 * 
	 * @param url
	 *            url link where icon is located.
	 * @return returns the image.
	 * @throws IOException
	 *             in case not capable of getting the image.
	 */
	public static BufferedImage readIcon(URL url) throws IOException {
		BufferedImage img = null;
		InputStream is = null;
		try {
			is = url.openStream();
			List<BufferedImage> imgs = ICODecoder.read(is);
			img = imgs != null ? imgs.size() > 0 ? imgs.get(0) : null : null;
		} finally {
			try {
				is.close();
			} catch (Exception exp) {
				logger.error("BufferedImage ERROR: " + exp);
			}
		}
		return img;
	}

	/**
	 * reads an image from the web
	 * 
	 * @param url
	 * @return
	 */
	public static BufferedImage readPngIcon(URL url) {
		BufferedImage img = null;
		try {
			img = ImageIO.read(url);
		} catch (IOException e) {
			logger.error("BufferedImage ERROR: " + e);
			e.printStackTrace();
		}
		return img;
	}
}
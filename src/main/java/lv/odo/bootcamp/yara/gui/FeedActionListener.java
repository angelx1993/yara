package lv.odo.bootcamp.yara.gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.swing.JDialog;

import org.apache.log4j.Logger;

import lv.odo.bootcamp.yara.EntityManager;
import lv.odo.bootcamp.yara.RefreshFeedThread;
import lv.odo.bootcamp.yara.entities.Feed;
import lv.odo.bootcamp.yara.entities.FeedEntry;

class FeedActionListener implements ActionListener {
	private static Logger logger = Logger.getLogger(FeedActionListener.class.getName());
	public static final String ACTION_ADD_FEED = "add_feed";
	public static final String ACTION_REMOVE_FEED = "remove_feed";
	public static final String ACTION_REFRESH_FEED = "refresh_feed";
	public static final String ACTION_REFRESH_ALL_FEEDS = "refresh_all_feeds";
	public static final String ACTION_RENAME_FEED = "rename_feed";
	public static final String ACTION_MARK_ALL_READ = "mark_all_feed_entries_as_read";

	private MainWindow parentWindow;

	public FeedActionListener(MainWindow mainWindow) {
		parentWindow = mainWindow;
	}

	/**
	 * Adds new RSS feed after pressing the button
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		String menuName = e.getActionCommand();
		switch (menuName) {
		case ACTION_ADD_FEED:
			JDialog dialog = new AddFeedDialog(parentWindow);
			dialog.setVisible(true);
			break;

		case ACTION_REMOVE_FEED:
			int selectedIndex = parentWindow.treeFeeds.getMinSelectionRow();
			try {
				EntityManager.deleteFeed(parentWindow.feedIdArray.get(selectedIndex - 1));
				parentWindow.loadFeeds();
				parentWindow.treeFeeds.setSelectionRow(selectedIndex - 1);
			} catch (ArrayIndexOutOfBoundsException ex) {
				logger.error("ACTION_REMOVE_FEED ERROR: " + ex);
				return;
			}
			break;

		case ACTION_REFRESH_FEED:
			int selectedIndexRefresh = parentWindow.treeFeeds.getMinSelectionRow();
			int id;
			try {
				id = parentWindow.feedIdArray.get(selectedIndexRefresh - 1);
			} catch (ArrayIndexOutOfBoundsException ex) {
				logger.error("ACTION_REFRESH_FEED ERROR: " + ex);
				return;
			}
			final Feed feed = EntityManager.getFeed(id);
			new Thread(new Runnable() {
				@Override
				public void run() {
					Thread refreshThread = new Thread(new RefreshFeedThread(feed));
					refreshThread.start();
					try {
						refreshThread.join();
					} catch (InterruptedException e) {
						logger.error("run ERROR: " + e);
						e.printStackTrace();
					}
					parentWindow.loadEntries();
				}
			}).start();
			break;

		case ACTION_REFRESH_ALL_FEEDS:
			List<Feed> list = EntityManager.getFeedList();
			ExecutorService executor = Executors.newCachedThreadPool();

			for (Feed feedI : list) {
				executor.submit(new RefreshFeedThread(feedI));
				parentWindow.loadEntries();
			}
			executor.shutdown();
			break;

		case ACTION_RENAME_FEED:
			JDialog dialogRename = new RenameFeedDialog(parentWindow);
			dialogRename.setVisible(true);
			break;

		case ACTION_MARK_ALL_READ:
			int index = parentWindow.treeFeeds.getMinSelectionRow();
			int id2;
			try {
				id2 = parentWindow.feedIdArray.get(index - 1);
			} catch (ArrayIndexOutOfBoundsException ex) {
				logger.error("ACTION_MARK_ALL_READ ERROR: " + ex);
				return;
			}
			Feed feedToRead = EntityManager.getFeed(id2);
			List<FeedEntry> entryList = EntityManager.getFeedEntryList(feedToRead);
			for (FeedEntry entry : entryList) {
				entry.setRead(true);
				EntityManager.saveFeedEntry(entry);
			}
			parentWindow.loadEntries();
			break;
		}
	}
}

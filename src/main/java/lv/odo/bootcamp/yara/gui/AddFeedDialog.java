package lv.odo.bootcamp.yara.gui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.Date;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import org.apache.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import lv.odo.bootcamp.yara.RefreshFeedThread;
import lv.odo.bootcamp.yara.entities.Feed;

public class AddFeedDialog extends JDialog implements ActionListener {
	private static final long serialVersionUID = -7923930052212315191L;
	private static Logger logger = Logger.getLogger(AddFeedDialog.class.getName());
	private final JPanel contentPanel = new JPanel();
	private JTextField txtUrl;
	private JButton okButton;
	private JButton cancelButton;
	private JFrame owner;

	/**
	 * Create the add new feed dialog form.
	 * 
	 * @param owner
	 *            parent window to be shoved in.
	 */
	public AddFeedDialog(JFrame owner) {
		super(owner, true);
		setTitle("Add New Feed");
		this.owner = owner;
		setSize(451, 163);
		setLocationRelativeTo(null);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(new BoxLayout(contentPanel, BoxLayout.Y_AXIS));
		JLabel lblNewLabel = new JLabel("Enter the feed URL you want to follow");
		contentPanel.add(lblNewLabel);
		Component verticalStrut = Box.createVerticalStrut(10);
		contentPanel.add(verticalStrut);
		txtUrl = new JTextField();
		txtUrl.setText("URL");
		contentPanel.add(txtUrl);
		txtUrl.setColumns(10);
		txtUrl.setMaximumSize(new Dimension(Integer.MAX_VALUE, txtUrl.getPreferredSize().height));
		JPanel buttonPane = new JPanel();
		buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
		getContentPane().add(buttonPane, BorderLayout.SOUTH);
		okButton = new JButton("Add");
		okButton.setActionCommand("Add");
		buttonPane.add(okButton);
		getRootPane().setDefaultButton(okButton);
		cancelButton = new JButton("Cancel");
		cancelButton.setActionCommand("Cancel");
		buttonPane.add(cancelButton);
		okButton.addActionListener(this);
		cancelButton.addActionListener(this);
	}

	/**
	 * processes the information entered in the input form.
	 */
	@Override
	public void actionPerformed(ActionEvent ae) {
		String action = ae.getActionCommand();
		if (action.equals("Add")) {
			String link = txtUrl.getText();

			try {
				Document doc = Jsoup.connect(link).get();
				org.jsoup.select.Elements links = doc.select("link[type=application/rss+xml]");

				if (links.size() > 0) {

					String rssLink = links.get(0).attr("abs:href").toString();
					link = rssLink;
				}
			} catch (IOException e1) {
				logger.error("actionPerformed rss not found ERROR: " + e1);
			} catch (IllegalArgumentException e) {
				logger.error("actionPerformed ERROR: " + e);
			}

			final Feed feed = new Feed();
			feed.setLastUpdated(new Date(0));
			feed.setFeedUrl(link);

			new Thread(new Runnable() {

				@Override
				public void run() {
					Thread t = new Thread(new RefreshFeedThread(feed));
					t.start();
					try {
						t.join();
					} catch (InterruptedException e) {
						logger.error("run ERROR: " + e);
						e.printStackTrace();
					}

					((MainWindow) owner).loadFeeds();
				}
			}).start();
		}

		dispose();
	}
}

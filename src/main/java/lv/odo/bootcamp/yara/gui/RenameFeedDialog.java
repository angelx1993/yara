package lv.odo.bootcamp.yara.gui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import lv.odo.bootcamp.yara.EntityManager;
import lv.odo.bootcamp.yara.entities.Feed;

public class RenameFeedDialog extends JDialog implements ActionListener {
	private static final long serialVersionUID = -7923930052212315191L;

	private final JPanel contentPanel = new JPanel();
	private JTextField txtName;
	private JButton okButton;
	private JButton cancelButton;
	private MainWindow owner;
	private Feed feedRename;

	/**
	 * Create the rename dialog. TODO: Make modal
	 * 
	 * @param owner
	 *            parent container.
	 */
	public RenameFeedDialog(MainWindow owner) {
		super(owner, true);
		setTitle("Rename Feed");
		this.owner = owner;
		setSize(451, 163);
		setLocationRelativeTo(null);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(new BoxLayout(contentPanel, BoxLayout.Y_AXIS));
		JLabel lblNewLabel = new JLabel("Enter feed name:");
		contentPanel.add(lblNewLabel);
		Component verticalStrut = Box.createVerticalStrut(10);
		contentPanel.add(verticalStrut);
		txtName = new JTextField();
		int selectedIndexRename = owner.treeFeeds.getMinSelectionRow();
		int idRename = owner.feedIdArray.get(selectedIndexRename - 1);
		feedRename = EntityManager.getFeed(idRename);
		txtName.setText(feedRename.getTitle());
		contentPanel.add(txtName);
		txtName.setColumns(10);
		txtName.setMaximumSize(new Dimension(Integer.MAX_VALUE, txtName.getPreferredSize().height));
		JPanel buttonPane = new JPanel();
		buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
		getContentPane().add(buttonPane, BorderLayout.SOUTH);
		okButton = new JButton("rename");
		okButton.setActionCommand("rename");
		buttonPane.add(okButton);
		getRootPane().setDefaultButton(okButton);
		cancelButton = new JButton("Cancel");
		cancelButton.setActionCommand("Cancel");
		buttonPane.add(cancelButton);
		okButton.addActionListener(this);
		cancelButton.addActionListener(this);
	}

	/**
	 * processes parsed data
	 */
	@Override
	public void actionPerformed(ActionEvent ae) {
		String action = ae.getActionCommand();
		if (action.equals("rename")) {
			String feedTitleAfter = txtName.getText();
			feedRename.setTitle(feedTitleAfter);
			EntityManager.saveFeed(feedRename);
			owner.loadFeeds();
		}
		dispose();
	}
}

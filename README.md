# YARA - Yet Another RSS Aggregator

## Introduction
Yara is a simple RSS Feed Aggregator which is easy to use and to understand. Its design is pleasant to look at and does not disappoint aesthetically.

## Features
Yara has variety of options:
Add RSS feed.
Delete RSS feed.
Refresh article list.
Mark all as read.

## Configuration
You need to have Java Runtime Environment installed in order to run it.

## Compiling and running
### Presequences
You will need this to be installed in order to build YARA:
* Java SDK (preferably OpenJDK) 1.7 or higher
* Apache Maven

### Build
To build YARA just download [latest version](https://gitlab.com/gim-/yara/repository/archive.tar.gz?ref=master) of the source code and run `mvn package` command. It will produce a **target/appassembler/bin** directory which contains executable **yara** shell script for \*nix systems and **yara.bat** for Microsoft Windows.

Also there will be an executable JAR file inside the **target** directory which contains all dependecies so you can distribute your YARA build easely and launch it everywhere.

## Usage
Using Yara is as simple as it could be. If you want to create a new RSS feed, just click on **Add feed**

![add.png](http://storage3.static.itmages.com/i/15/0728/h_1438081946_2593594_eda442f5d1.png)

and insert the web link you want to include as your feed choice.

![Add New Feed_008.png](http://storage1.static.itmages.com/i/15/0728/h_1438082444_3667130_de8e6093fd.png)





When you want to check for new articles in any feed **Refresh Feed**.

![refresh.png](http://storage2.static.itmages.com/i/15/0728/h_1438082629_2521438_2061fb705d.png) or, to refresh every feed ![refreshAll.png](http://storage3.static.itmages.com/i/15/0728/h_1438082261_6888049_83bbb27dfb.png)

If you want to **delete a feed**, choose your target and press  click on **Remove feed**, or press your mouses right botton and Delete Feed.

![Remove.png](http://storage4.static.itmages.com/i/15/0728/h_1438082558_8741245_297a281d86.png)


![list2.png](http://storage2.static.itmages.com/i/15/0729/h_1438153701_7821189_285003602e.png)

Or you might as well marck all entries as read, by clicking on mark all as read.

In order to **show articles from specific feed** go to the left of the Yara screen,  where you can see map tree, which contains feeds, and **click** on one you want to explore.

![list1.png](http://storage4.static.itmages.com/i/15/0729/h_1438153857_4075233_99f6911f8f.png)

To **see an article** click on title you are interested in and pay your attention to the window just under the table. You can easily **resize the window** by clicking on border you want and grag the window in the size you like, or if you prefer to **visit site**, from which the entry is coming from  and read it there, just above the article you can see hyperlink by clicking on it, your web browser will open article location.

![Add New Feed_008.png](http://storage4.static.itmages.com/i/15/0729/h_1438154292_9449032_ef4a7d8378.png)
